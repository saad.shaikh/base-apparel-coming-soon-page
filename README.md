# Title: Base Apparel coming soon page

Tech stack: Elm, nodemon & Shell Script

Deployed project: https://saad-shaikh-base-apparel-coming-soon-page.netlify.app/

## Main tasks:
- Created a web page using Elm instead of HTML and JavaScript
- Used nodemon and Shell Script to run the application in the developer mode such that it updates every time a file is saved
- Used Shell Script to create a command that links the web page to it's CSS document
- Created an error message for invalid inputs with only CSS


## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
