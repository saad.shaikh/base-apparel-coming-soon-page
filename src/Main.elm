module Main exposing (main)
import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

--INIT
init = { inputValue = "" }

--VIEW
view param  =
    div [ class "app"]
        [ img 
            [ class "logo"
            , src "media/logo.svg"
            , alt "Base Apparel"
            , attribute "role" "heading"
            , attribute "aria-level" "1"
            ] []
        , div [ class "hero", attribute "role" "banner" ] []
        , section [ class "text", attribute "role" "main" ]
            [ h1 [ class "h1", attribute "role" "heading", attribute "aria-level" "2" ]
                [ span [ class "h1-top" ] [ text "WE'RE" ]
                , span [ class "h1-middle" ] [ text "COMING" ]
                , span [ class "h1-bottom" ] [ text "SOON" ] 
                ]
            , p [ class "desc", attribute "role" "contentinfo" ]
                [ text "Hello fellow shoppers! We're currently building our new fashion store. Add your email below to stay up-to-date with announcements and our launch deals."
                ]
            , Html.form [ class "form" ]
                [ input 
                    [ class "input", placeholder "Email Address", type_ "email", required True ] []
                , label [ class "error" ] [ text "Please provide a valid email" ]
                , input [ class "submit", type_ "submit" ] []
                ]

            , footer [ class "attribution" ]
                [ text "Challenge by "
                , a [ href "https://www.frontendmentor.io?ref=challenge", target "_blank" ]
                    [ text "Frontend Mentor" ]
                , text ". Coded by "
                , a [ href "https://saad-shaikh-portfolio.netlify.app/", target "_blank" ]
                    [ text "Saad Shaikh" ]
                , text "." ]
            ]
        ]
    

--UPDATES
update : a -> a
update param = param

--MAIN
main = 
    Browser.sandbox
    { init=init
    , view=view
    , update=update
    }